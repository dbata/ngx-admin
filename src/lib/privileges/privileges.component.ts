import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { groupBy, intersection } from 'lodash';
import { AdminService } from '../admin.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-privileges-list',
  templateUrl: './privileges.component.html',
  styleUrls: ['./privileges.component.scss'],
})
export class PrivilegesComponent implements OnInit, OnDestroy {
  public privileges: any[];
  private initialPrivileges: any[];
  private filteredByCategory: any[] = [];
  private filteredBySearchTerm: any[] = [];
  public lastMessage: {
    Title: string;
    Description: string;
  };
  public search$: Subject<string> = new Subject();
  public searchTags: any[] = [];
  public distinctTags: Set<string> = new Set<string>();
  private searchSubscription: Subscription;
  constructor(
    private readonly _context: AngularDataContext,
    private readonly _errorService: ErrorService,
    private readonly _translate: TranslateService,
    private readonly _loading: LoadingService,
    private readonly _adminService: AdminService
  ) {}

  async ngOnInit() {
    try {
      this._loading.showLoading();
      // get all available privileges
      let privileges = await this._context
        .model('Privileges')
        .asQueryable()
        .expand('tags($select=id, name)')
        .take(-1)
        .getItems();
      const noTagItem = {
        id: null,
        name: this._translate.instant('Admin.NoTags'),
      };
      // group them by tags
      privileges = groupBy(privileges, (item: { tags?: any[] }) => {
        if (Array.isArray(item.tags) && item.tags.length) {
          item.tags.forEach((tag: any) => {
            // hold distinct tags via a set
            this.distinctTags.add(JSON.stringify(tag));
          });
          return [JSON.stringify(item.tags)];
        } else {
          // if at least one object does not have tags
          // add the noTagItem
          this.distinctTags.add(JSON.stringify(noTagItem));
        }
      });
      // extract values from set to array
      this.searchTags = Array.from(this.distinctTags).map((searchTag: string) =>
        JSON.parse(searchTag)
      );
      for (const [_, values] of Object.entries(privileges)) {
        // construct tag description
        // for privileges that belong to multiple tags
        // or for those that do not have any tags
        (values as any[]).forEach((item: any) => {
          item.tagDescription =
            Array.isArray(item.tags) && item.tags.length
              ? item.tags
                  .map((tag: { name: string }) => tag.name)
                  .join(` ${this._translate.instant('Admin.And')} `)
              : this._translate.instant('Admin.NoTags');
          // also construct readable mask representation
          item.readableMask = this._adminService
            .getConfigurableActions(item.mask)
            .map((action: string) =>
              this._translate.instant(
                `Admin.Permissions.Types.${action.toLowerCase()}`
              )
            )
            .join(', ');
        });
      }
      // and keep only the values of the grouped object
      this.privileges = this.initialPrivileges = Object.values(privileges);
      // perform a very simple local search
      this.searchSubscription = this.search$
        .pipe(debounceTime(300), distinctUntilChanged())
        .subscribe((searchText: string) => {
          if (!(this.initialPrivileges && this.initialPrivileges.length)) {
            return;
          }
          if (!searchText) {
            // restore view
            this.privileges =
              this.filteredByCategory.length > 0
                ? this.validateSearches(
                    this.initialPrivileges,
                    this.filteredByCategory
                  )
                : this.initialPrivileges;
            this.filteredBySearchTerm = [];
          } else {
            const targets = this.initialPrivileges.map((privilege) => {
              return privilege.filter(
                (item: {
                  appliesTo: string;
                  alternateName: string;
                  readableMask: string;
                  name: string;
                  description: string;
                }) =>
                  item.appliesTo.includes(searchText) ||
                  item.alternateName.includes(searchText) ||
                  item.readableMask.includes(searchText) ||
                  item.name.includes(searchText) ||
                  item.description.includes(searchText)
              );
            });
            this.filteredBySearchTerm = targets;
            this.privileges =
              this.filteredByCategory.length > 0
                ? this.validateSearches(targets, this.filteredByCategory)
                : targets;
            // empty privileges if every item is empty (e.g. no results found)
            if (this.privileges.every((item) => item.length === 0)) {
              this.privileges = [];
              this.filteredBySearchTerm = [];
            }
          }
        });
    } catch (err) {
      // log error
      console.error(err);
      // handle 404 as an unsupported feature
      if ((err as HttpErrorResponse).status === 404) {
        this.lastMessage = this._translate.instant('Admin.PrivilegesNotFound');
      } else {
        // else, just show the error via error service
        this._errorService.showError(err);
      }
    } finally {
      this._loading.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  onKey(event: any) {
    if (event && event.target) {
      this.search$.next(event.target.value);
    }
  }

  onSelectTag(event: any) {
    if (!(event && event.target && this.initialPrivileges)) {
      return;
    }
    let targetTag: number | null;
    // note: event.target.value is always a string
    if (event.target.value === 'null') {
      targetTag = null;
    } else {
      targetTag = parseInt(event.target.value, 10);
    }
    // targetTag 0 is for '-'
    if (targetTag === 0) {
      // restore privileges
      this.privileges =
        this.filteredBySearchTerm.length > 0
          ? this.validateSearches(
              this.initialPrivileges,
              this.filteredBySearchTerm
            )
          : this.initialPrivileges;
      this.filteredByCategory = [];
      return;
    }
    const privileges =
      // filter items by tag id
      this.initialPrivileges.map((privilege: any) => {
        return privilege.filter((item: { tags: any[] }) => {
          return targetTag == null
            ? (Array.isArray(item.tags) && item.tags.length === 0) ||
                item.tags == null
            : Array.isArray(item.tags) &&
                item.tags.find((tag: { id: number }) => tag.id === targetTag);
        });
      });
    this.filteredByCategory = privileges;
    this.privileges =
      this.filteredBySearchTerm.length > 0
        ? this.validateSearches(privileges, this.filteredBySearchTerm)
        : privileges;
    // empty privileges if every item is empty (e.g. no results found)
    if (this.privileges.every((item) => item.length === 0)) {
      this.privileges = [];
      this.filteredByCategory = [];
    }
  }

  validateSearches(source: any[], target: any[]): any[] {
    if (source.length !== target.length) {
      return [];
    }
    const result = [];
    for (let i = 0; i < source.length; i++) {
      result.push(intersection(source[i], target[i]));
    }
    return result;
  }
}
