import { Component, AfterViewInit, ViewChild} from '@angular/core';
// @ts-ignore
import {group_list} from './list';
import {AdvancedTableComponent, AdvancedTableConfiguration, TableConfiguration} from '@universis/ngx-tables';
@Component({
  selector: 'lib-groups-list',
  templateUrl: './groups-list.component.html'

})
export class GroupsListComponent implements AfterViewInit {
  @ViewChild('table') table: AdvancedTableComponent;
  public filter: any = {};
  constructor() { }

  ngAfterViewInit() {
    if (this.table) {
      const config = JSON.parse(JSON.stringify(group_list));
      const editBtn = config.columns.find(col => col.name === 'id');
      // assign the appropriate base route to the edit button
      editBtn.formatOptions.commands[0] = '..';
      //  assign the configuration to the table and fetch the data
      this.table.config = AdvancedTableConfiguration.cast(config);
      this.table.fetch(true);
    }
  }


}
