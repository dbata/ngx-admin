/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const users_list = {
  "title": "Admin.Users.All",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["../../..", "users", "${id}", "edit"]
      }
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Groups.Name"
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Groups.AlternateName"
    },
    {
      "name": "dateCreated",
      "property": "dateCreated",
      "title": "Admin.Users.DateCreated",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    },
    {
      "name": "dateModified",
      "property": "dateModified",
      "title": "Admin.Users.DateModified",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    }
  ],
  "criteria": [
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "paths": [
  ]
}
