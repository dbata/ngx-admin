/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const DEFAULT_GROUP_PRIVILEGES_LIST = {
  columns: [
    {
      name: "entityType",
      property: "entityType",
      title: "Admin.AppliesTo",
    },
    {
      name: "title",
      property: "title",
      title: "Admin.PrivilegeDescription",
    },
    {
      name: "type",
      property: "type",
      title: "Admin.PrivilegeType",
      formatter: "TranslationFormatter",
      formatString: "Admin.DefaultPrivilegeTypes.${value}",
    },
    {
      name: "filter",
      property: "filter",
      title: "Admin.PrivilegeFilter",
    },
    {
      name: "mask",
      property: "mask",
      title: "Admin.PrivilegeActions",
      formatter: "PermissionMaskFormatter",
    },
  ],
};
