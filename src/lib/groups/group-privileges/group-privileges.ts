/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const GROUP_PRIVILEGES_LIST = {
  searchExpression:
    "indexof(privilege, '${text}') ge 0 or indexof(parentPrivilege, '${text}') ge 0",
  selectable: true,
  multipleSelect: true,
  columns: [
    {
      name: "id",
      property: "id",
      hidden: true,
    },
    {
      name: "privilege",
      property: "privilege",
      title: "Admin.Privilege",
    },
    {
      name: "parentPrivilege",
      property: "parentPrivilege",
      title: "Admin.ParentPrivilege",
      formatter: "TemplateFormatter",
      formatString: "${parentPrivilege ? parentPrivilege : '-'}",
    },
    {
      name: "target",
      property: "target",
      title: "Admin.Target",
      formatters: [
        {
          formatter: "TemplateFormatter",
          formatString: "${target == 0 ? 'Admin.AllTargets' : 'ID: ' + target}",
        },
        {
          formatter: "TranslationFormatter",
          formatString: "${value}",
        },
      ],
    },
    {
      name: "mask",
      property: "mask",
      title: "Admin.PrivilegeActions",
      formatter: "PermissionMaskFormatter",
    },
  ],
};
