import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppEventService,
  LoadingService,
  ModalService,
  ToastService,
} from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdminService } from '../../../admin.service';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';

enum Targets {
  'All' = '0',
}

enum States {
  'Insert' = 1
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-add-group-privilege',
  templateUrl: './add-group-privilege.component.html',
})
export class AddGroupPrivilegeComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy {
  public lastError: any;
  @Input() privileges: any[];
  @Input() account: number | string;
  @ViewChild('lastErrorRef') lastErrorRef!: ElementRef;
  public isProcessing: boolean;
  public maskMap = {
    permissions: ['read', 'create', 'update', 'delete', 'execute'],
    masks: [1, 2, 4, 8, 16],
  };
  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _adminService: AdminService,
    private _translate: TranslateService,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
    private _toastService: ToastService,
    private _loading: LoadingService
  ) {
    super(router, activatedRoute);
    // set modal title
    this.modalTitle = 'Admin.DefineActions';
  }

  async ok() {
    try {
      this._loading.showLoading();
      if (this.account == null) {
        return;
      }
      // clear last error
      this.lastError = null;
      // validate privileges
      const validPrivileges = this.privileges.every((privilege) =>
        !!Object.keys(privilege).find(
          (key) =>
            this.maskMap.permissions.includes(key) && privilege[key] === true
        )
      );
      if (!validPrivileges) {
        throw new Error(this._translate.instant('Admin.MustDefineActions'));
      }
      // encode actions to mask
      const savePrivileges = this._adminService
        .encodePermissionsToMasks(this.privileges, this.maskMap)
        .map((item) => {
          // and prepare clean payload
          return {
            account: this.account,
            privilege: item.alternateName,
            parentPrivilege: item.parentPrivilege || null,
            target: Targets.All,
            mask: item.mask,
            $state: States.Insert,
          };
        });
      // save privileges
      await this._context.model('Users/Permissions').save(savePrivileges);
      // fire app event
      this._appEvent.add.next({
        action: 'AddedPermissions',
        target: this.account,
      });
      // show toast
      const toastMessage: {
        Title: string;
        Message: string;
      } = this._translate.instant('Admin.AddPrivileges.SuccessToast');
      this._toastService.show(toastMessage.Title, toastMessage.Message);
      // and close
      return this.cancel();
    } catch (err) {
      console.error(err);
      this.lastError = err;
      // scroll to error ref
      setTimeout(() => {
        this.scrollToErrorRef();
      }, 0);
    } finally {
      this._loading.hideLoading();
    }
  }

  async cancel() {
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  ngOnInit() {
    try {
      this.isProcessing = true;
      if (!(this.privileges && this.privileges.length)) {
        return;
      }
      this.privileges.forEach((item) => {
        item.configurableActions = this._adminService
          .getConfigurableActions(item.mask)
          .map((action: string) => action.toLowerCase());
      });
    } catch (err) {
      console.error(err);
    } finally {
      this.isProcessing = false;
    }
  }

  ngOnDestroy() {}

  private scrollToErrorRef(): void {
    if (
      this.lastErrorRef &&
      this.lastErrorRef.nativeElement &&
      typeof this.lastErrorRef.nativeElement.scrollIntoView === 'function'
    ) {
      this.lastErrorRef.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
      });
    }
  }
}
