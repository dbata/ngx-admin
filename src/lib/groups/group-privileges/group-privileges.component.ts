import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { GROUP_PRIVILEGES_LIST } from './group-privileges';
import { SPECIAL_GROUP_PRIVILEGES_LIST } from './special-group-privileges';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import {
  AdvancedRowActionComponent,
  AdvancedSelectService,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent,
} from '@universis/ngx-tables';
import {
  AppEventService,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
} from '@universis/common';
import { AddGroupPrivilegeComponent } from './add-group-privilege/add-group-privilege.component';
import { ClientDataQueryable } from '@themost/client';
import { AngularDataContext } from '@themost/angular';
import { SELECT_PRIVILEGES_CONFIG } from './add-group-privilege/select-privileges';
import { DefaultGroupPrivilegesComponent } from './default-group-privileges/default-group-privileges.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-group-privileges',
  templateUrl: './group-privileges.component.html',
})
export class GroupPrivilegesComponent implements AfterViewInit, OnDestroy {
  public tableConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  private paramsSubscription: Subscription;
  private appEventSubscription: Subscription;
  public recordsTotal: number;
  public account: number | string;
  private selectedItems: any[] = [];
  private selectConfiguration = AdvancedTableConfiguration.cast(
    SELECT_PRIVILEGES_CONFIG
  );
  public radioOptions = [
    {
      id: 'Permissions',
      title: 'Admin.DefaultPrivilegeTypes.globalPlural',
      checked: true,
    },
    {
      id: 'SpecialPermissions',
      title: 'Admin.DefaultPrivilegeTypes.selfPlural',
      checked: false,
    },
  ];
  public targetModel = 'Users/Permissions';

  public limitedAccessGroup: boolean;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _selectService: AdvancedSelectService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _appEvent: AppEventService,
    private _loadingService: LoadingService,
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _toastService: ToastService
  ) {}

  async ngAfterViewInit() {
    this.paramsSubscription = this._activatedRoute.parent.params.subscribe(
      async (params) => {
        try {
          this.account = params.id;
          this.tableConfiguration = AdvancedTableConfiguration.cast(
            GROUP_PRIVILEGES_LIST,
            true
          );
          // set model
          this.tableConfiguration.model = `Accounts/${this.account}/Permissions`;
          // check if the group permissions can be modified
          // let's assume correct api version here and not check metadata for groupAttributes
          const group = await this._context
            .model('Groups')
            .where('id')
            .equal(this.account)
            .expand('groupAttributes')
            .select('id')
            .getItem();
          this.limitedAccessGroup = !!(
            group &&
            Array.isArray(group.groupAttributes) &&
            group.groupAttributes.find(
              (attribute: string) => attribute === 'limitedAccess'
            )
          );
          if (this.limitedAccessGroup) {
            // remove table selection and actions
            this.tableConfiguration.selectable =
              this.tableConfiguration.multipleSelect = false;
          }
          // set config and init
          this.table.config = this.tableConfiguration;
          this.table.ngOnInit();
        } catch (err) {
          console.error(err);
          this._errorService.showError(err, {
            continueLink: '.',
          });
        }
      }
    );
    this.appEventSubscription = this._appEvent.added.subscribe((event) => {
      try {
        if (
          event &&
          event.action === 'AddedPermissions' &&
          event.target === this.account
        ) {
          // reload table data
          this.table.fetch(true);
        }
      } catch (err) {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.',
        });
      }
    });
    try {
      // get distinct tags
      const tags = await this._context
        .model('Privileges')
        .select('tags/id as id, tags/name as name')
        .groupBy('tags/id', 'tags/name')
        .take(-1)
        .getItems();
      if (Array.isArray(tags) && tags.length) {
        // populate privilege selection filters
        this.selectConfiguration.searches = tags.map((tag) => {
          return {
            name: tag.name,
            value: tag.id,
            filter: {
              tag: tag.id,
            },
            emit: false,
          };
        });
      }
    } catch (err) {
      // just log error
      console.error(err);
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy() {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.appEventSubscription) {
      this.appEventSubscription.unsubscribe();
    }
  }

  addPrivileges() {
    if (this.targetModel === 'Users/Permissions') {
      return this.addGlobalPrivileges();
    }
    return this.copySelfPrivileges();
  }

  async copySelfPrivileges() {
    try {
      this._loadingService.showLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: [{}],
          data: {
            group: this.account,
          },
          modalTitle: 'Admin.CopySelfPrivilegesAction.Title',
          description: 'Admin.CopySelfPrivilegesAction.Description',
          errorMessage: 'Admin.CopySelfPrivilegesAction.CompletedWithErrors',
          formTemplate: 'Permissions/copySelfPrivileges',
          refresh: this.refreshAction,
          execute: this.executeCopySelfPrivileges(),
        },
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeCopySelfPrivileges() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1,
      });
      const result = {
        total: 1,
        success: 0,
        errors: 0,
      };
      // handle fake progress with interval
      let progressValue = 2;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 2 < 100 ? progressValue + 2 : 5;
        this.refreshAction.emit({
          progress: progressValue,
        });
      }, 1000);
      // get source account from modal component
      const component = this._modalService.modalRef
        .content as AdvancedRowActionComponent;
      const data =
        component &&
        component.formComponent &&
        component.formComponent.form &&
        component.formComponent.form.formio &&
        component.formComponent.form.formio.data;
      if (!data.sourceAccount) {
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        result.errors = result.total;
        return observer.next(result);
      }
      this._context
        // copy self privilege from source account
        .model(`Accounts/${data.sourceAccount}/copySelfPrivileges`)
        .save({
          // to base (target) account
          account: this.account,
        })
        .then(() => {
          // reload table
          this.table.fetch(true);
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          // show success toast message
          const toastMessage: {
            Title: string,
            Message: string
          } = this._translateService.instant('Admin.CopySelfPrivilegesAction.SuccessToast');
          this._toastService.show(toastMessage.Title, toastMessage.Message);
          // and return result
          return observer.next(result);
        })
        .catch((err) => {
          console.error(err);
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          // and return result with errors
          result.errors = result.total;
          return observer.next(result);
        });
    });
  }

  addGlobalPrivileges() {
    this._selectService
      .select({
        modalTitle: 'Admin.Permissions.SelectPrivilege',
        tableConfig: this.selectConfiguration,
      })
      .then((actionResult) => {
        if (
          !(
            actionResult &&
            actionResult.result === 'ok' &&
            actionResult.items &&
            actionResult.items.length
          )
        ) {
          return;
        }
        return this._modalService.openModalComponent(
          AddGroupPrivilegeComponent,
          {
            class: 'modal-xl',
            keyboard: false,
            ignoreBackdropClick: true,
            initialState: {
              privileges: actionResult.items,
              account: this.account,
            },
          }
        );
      })
      .catch((err) => {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.',
        });
      });
  }

  public showDefaultPrivileges() {
    try {
      this._modalService.openModalComponent(DefaultGroupPrivilegesComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: { account: this.account },
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  async removePrivileges() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Admin.RemovePrivileges.Title',
          description: 'Admin.RemovePrivileges.Description',
          errorMessage: 'Admin.RemovePrivileges.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeRemovePrivileges(),
        },
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeRemovePrivileges() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1,
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      // execute update for all items (transactional update)
      // map items
      const removePrivileges = this.selectedItems.map((item) => {
        return {
          id: item.id,
          $state: 4,
        };
      });
      // handle fake progress with interval
      let progressValue = 2;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 2 < 100 ? progressValue + 2 : 2;
        this.refreshAction.emit({
          progress: progressValue,
        });
      }, 1000);
      this._context
        .model(this.targetModel)
        .save(removePrivileges)
        .then(() => {
          // reload table
          this.table.fetch(true);
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          // and return result
          return observer.next(result);
        })
        .catch((err) => {
          console.error(err);
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          // and return result with errors
          result.errors = result.total;
          return observer.next(result);
        });
    });
  }

  async editPrivilegeMask() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Admin.EditPrivilegeMask.Title',
          description: 'Admin.EditPrivilegeMask.Description',
          errorMessage: 'Admin.EditPrivilegeMask.CompletedWithErrors',
          formTemplate: 'Permissions/editMask',
          refresh: this.refreshAction,
          execute: this.executeEditPrivilegeMask(),
        },
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeEditPrivilegeMask() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1,
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      // handle fake progress with interval
      let progressValue = 2;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 2 < 100 ? progressValue + 2 : 2;
        this.refreshAction.emit({
          progress: progressValue,
        });
      }, 1000);
      // get actions from modal component
      const component = this._modalService.modalRef
        .content as AdvancedRowActionComponent;
      const data =
        component &&
        component.formComponent &&
        component.formComponent.form &&
        component.formComponent.form.formio &&
        component.formComponent.form.formio.data;
      if (!(data && Array.isArray(data.actions) && data.actions.length > 0)) {
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        result.errors = result.total;
        return observer.next(result);
      }
      const newMask = data.actions.reduce((acc: number, next: number) => acc | next);
      const updateItems = [];
      // assign new mask to selected items
      this.selectedItems.forEach((item) => {
        if (item.mask !== newMask) {
          updateItems.push({
            id: item.id,
            mask: newMask,
          });
        }
      });
      this._context.model(this.targetModel).save(updateItems).then(() => {
          // reload table
          this.table.fetch(true);
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          // and return result
          return observer.next(result);
      })        .catch((err) => {
        console.error(err);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'mask'];
          // query items
          const queryItems = await lastQuery.select
            .apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter((item) => {
              return (
                this.table.unselected.findIndex((x) => {
                  return x.id === item.id;
                }) < 0
              );
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              mask: item.mask
            };
          });
        }
      }
    }
    return items;
  }

  public onListSelection(event: any): void {
    try {
      if (!(event && event.id)) {
        return;
      }
      switch (event.id) {
        case 'Permissions':
          // set config
          this.tableConfiguration = AdvancedTableConfiguration.cast(
            GROUP_PRIVILEGES_LIST,
            true
          );
          // set model
          this.tableConfiguration.model = `Accounts/${this.account}/Permissions`;
          // set target model for actions
          this.targetModel = 'Users/Permissions';
          break;
        case 'SpecialPermissions':
          // set config
          this.tableConfiguration = AdvancedTableConfiguration.cast(
            SPECIAL_GROUP_PRIVILEGES_LIST,
            true
          );
          // set model
          this.tableConfiguration.model = `Accounts/${this.account}/SpecialPermissions`;
          // set target model for actions
          this.targetModel = 'Accounts/SpecialPermissions';
          break;
        default:
          return;
      }
      if (this.limitedAccessGroup) {
        // remove table selection and actions
        this.tableConfiguration.selectable =
          this.tableConfiguration.multipleSelect = false;
      }
      // set config and init
      this.table.config = this.tableConfiguration;
      this.table.reset(true);
      // reset search text
      this.advancedSearch.text = null;
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }
}
