/* tslint:disable quotemark */
export const AUTHORIZED_ACCOUNTS_CONFIG = {
    "title": "Authorized Accounts",
    "model": "offline",
    "selectable": true,
    "multipleSelect": true,
    "columns": [
      {
        "name": "id",
        "property": "id",
        "title": "AdditionalType",
        "hidden": true
      },
      {
        "name": "name",
        "property": "name",
        "title": "Name",
        "hidden": true
      },
      {
        "name": "additionalType",
        "property": "additionalType",
        "title": "AdditionalType",
        "hidden": true
      },
      {
        "name": "accountName",
        "property": "account.name",
        "title": "Admin.Permissions.Name"
      },
      {
        "name": "accountAdditionalType",
        "property": "account.additionalType",
        "title": "Admin.Permissions.AdditionalType"
      }
    ],
    "defaults": {
    }
  };
