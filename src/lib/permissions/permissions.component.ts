import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedSelectService,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableEditorDirective,
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { AdminService } from '../admin.service';
import { AUTHORIZED_ACCOUNTS_CONFIG } from './authorized-accounts.config';
import { ACCOUNTS_CONFIG } from './accounts.config';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import {
  DIALOG_BUTTONS,
  ErrorService,
  ModalService,
  ToastService,
  LoadingService,
} from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-permission-management',
  templateUrl: './permissions.component.html',
})
export class PermissionsComponent implements OnInit, OnDestroy {
  public privilegeName: string;
  public privilege: any;
  public privilegeDescription: string;
  public target: number | string;
  public permissionsList: string[];
  public config: AdvancedTableConfiguration;
  @ViewChild('accounts') accounts: AdvancedTableComponent;
  @ViewChild(AdvancedTableEditorDirective)
  tableEditor: AdvancedTableEditorDirective;
  private dataSubscription: Subscription;
  private subscription: Subscription;
  public privilegedAccounts: any[];
  public recordsTotal: any;
  public readonly accountsConfig = ACCOUNTS_CONFIG;
  public initialAccounts: any[];
  public isAvailable = true;
  public isLoading = true;
  public targetDescription: string;
  public maskMap = {
    permissions: ['read', 'create', 'update', 'delete', 'execute'],
    masks: [1, 2, 4, 8, 16],
  };
  public lastMessage: {
    Title: string,
    Description: string
  };
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _adminService: AdminService,
    private _selectService: AdvancedSelectService,
    private _translateService: TranslateService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _loadingService: LoadingService,
    private _context: AngularDataContext
  ) {}

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(
      async (params) => {
        try {
          // set loading
          this._loadingService.showLoading();
          this.isLoading = true;
          // clear last message
          this.lastMessage = null;
          if (params.privilege == null) {
            return this._errorService.showError(
              new Error('Expected a privilege router parameter.')
            );
          }
          // get privilege
          const privilege = await this._context
            .model('Privileges')
            .where('id')
            .equal(params.privilege)
            .getItem();
          // and validate it
          if (privilege == null) {
            this.lastMessage = this._translateService.instant(
              'Admin.InvalidPrivilege'
            );
            return;
          }
          if (params.target) {
            this.target = params.target;
          } else {
            // if target is not present in the route, use 0
            this.target = 0;
          }
          this.privilege = privilege;
          // extract name and description
          this.privilegeName = this.privilege.alternateName;
          this.privilegeDescription = privilege.description || privilege.name;
          // construct available actions
          this.permissionsList = this._adminService
            .getConfigurableActions(privilege.mask)
            .map((item) => item.toLowerCase());
          // get relevant accounts
          this.privilegedAccounts =
            await this._adminService.getPrivilegedAccounts(
              this.privilegeName,
              this.target
            );
          // decode masks
          this.privilegedAccounts = this._adminService.decodeMasksToPermissions(
            this.privilegedAccounts,
            this.maskMap,
            this.permissionsList
          );
          // deep copy initial state (for undo)
          this.initialAccounts = JSON.parse(
            JSON.stringify(this.privilegedAccounts)
          );
          this.config = cloneDeep(AUTHORIZED_ACCOUNTS_CONFIG);
          // set config
          this.accounts.config = AdvancedTableConfiguration.cast(this.config);
          // set permission columns for table
          const permissionColumns = [];
          this.permissionsList.forEach((permission) => {
            const permissionTitle = this._translateService.instant(
              `Admin.Permissions.Types.${permission}`
            );
            permissionColumns.push({
              name: permission,
              title: permissionTitle,
              property: permission,
              formatter: 'CheckBoxFormatter',
            });
          });
          // keep lastNonPermissionIndex (+1 for selectable checkbox)
          const lastNonPermissionIndex =
            this.accounts.config.columns.length + 1;
          // push permission columns
          this.accounts.config.columns.push(...permissionColumns);
          this.accounts.ngOnInit();
          // set items
          this.tableEditor.set(this.privilegedAccounts);
          this._loadingService.hideLoading();
          this.isLoading = false;
          // listen for changes on permissions
          this.accounts.dataTable.on('change', '.permissions', (change) => {
            // split id
            const idSplit = change.target.id.replace('s', '').split('-');
            // get account
            const account = idSplit[2];
            // get permission
            const permission = idSplit[1];
            // get permissionName
            const permissionName =
              this.permissionsList[permission - lastNonPermissionIndex];
            // get and set value
            const value = change.target.checked;
            this.privilegedAccounts[account][permissionName] = value;
            // set table editor as dirty
            this.tableEditor.dirty = true;
          });
          this.privilegedAccounts.forEach((account) => {
            // used to update existing account masks
            account.toBeUpdated = true;
          });
        } catch (err) {
          // log error
          console.error(err);
          // handle 404 as an unsupported feature
          if ((err as HttpErrorResponse).status === 404) {
            this.lastMessage = this._translateService.instant(
              'Admin.PrivilegesNotFound'
            );
          } else {
            // else, just show the error via error service
            this._errorService.showError(err);
          }
        } finally {
          this._loadingService.hideLoading();
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  add() {
    this._selectService
      .select({
        modalTitle: this._translateService.instant(
          'Admin.Permissions.AddAccount'
        ),
        tableConfig: this.accountsConfig,
      })
      .then((result) => {
        if (result.result === 'ok') {
          let items = result.items;
          items.forEach((item) => {
            // check for any already present items
            const findItem = this.privilegedAccounts.findIndex(
              (account) => account.account.name === item.name
            );
            if (findItem > -1) {
              const index = items.indexOf(item);
              items.splice(index, 1);
            }
          });
          // check if read is present in permissions list
          const findIndex = this.permissionsList.findIndex(
            (permission) => permission === 'read'
          );
          items.forEach((item) => {
            // set mask to 1 (read)
            if (findIndex > -1) {
              item.mask = 1;
            }
            // sync with authorized-accounts config
            Object.defineProperty(item, 'account', {
              enumerable: true,
              writable: true,
              value: {
                name: item.name,
                additionalType: item.additionalType,
              },
            });
          });
          if (items.length !== 0) {
            // decode masks
            items = this._adminService.decodeMasksToPermissions(
              items,
              this.maskMap,
              this.permissionsList
            );
            // add items
            this.tableEditor.add(...items);
            // keep the sync on main array (to handle checkbox checking events)
            this.privilegedAccounts = this.privilegedAccounts.concat(items);
          }
        }
      });
  }

  apply() {
    this._modalService
      .showInfoDialog(
        this._translateService.instant('Admin.Permissions.ApplyChanges'),
        this._translateService.instant('Admin.Permissions.ApplyChangesMessage'),
        DIALOG_BUTTONS.OkCancel
      )
      .then(async (result) => {
        if (result === 'ok') {
          try {
            this._loadingService.showLoading();
            // get removed items
            const removedItems = Array.from(
              this.accounts.dataTable.rows('.removed').data()
            );
            removedItems.forEach((account) => {
              const findIndex = this.privilegedAccounts.findIndex(
                (acc) => acc.account.name === account['account'].name
              );
              if (findIndex > -1) {
                this.privilegedAccounts.splice(findIndex, 1);
              }
            });
            if (removedItems.length !== 0) {
              // remove items, if any
              try {
                await this._adminService.removeAccounts(removedItems);
              } catch (err) {
                this._errorService.showError(err, {
                  continueLink: '.',
                });
              }
            }
            if (this.privilegedAccounts.length !== 0) {
              // encode masks
              this.privilegedAccounts =
                this._adminService.encodePermissionsToMasks(
                  this.privilegedAccounts,
                  this.maskMap
                );
              // save items
              await this._adminService.saveAccounts(
                this.privilegedAccounts,
                this.privilegeName,
                this.target
              );
              this._toastService.show(
                this._translateService.instant(
                  'Admin.Permissions.ApplyChanges'
                ),
                this._translateService.instant(
                  'Admin.Permissions.ApplyChangesToastMessage'
                )
              );
            }
            // fetch items
            this.fetchItems();
            this.tableEditor.dirty = false;
          } catch (err) {
            console.error(err);
            this._errorService.showError(err, {
              continueLink: '.',
            });
          } finally {
            this._loadingService.hideLoading();
          }
        }
      });
  }

  remove() {
    try {
      // keep the sync on main array (to handle checkbox checking events)
      this.accounts.selected.forEach((account) => {
        if (!account.hasOwnProperty('toBeUpdated')) {
          const findIndex = this.privilegedAccounts.findIndex(
            (acc) => acc.account.name === account.account.name
          );
          if (findIndex > -1) {
            this.privilegedAccounts.splice(findIndex, 1);
          }
        }
      });
      // remove items
      this.tableEditor.remove(...this.accounts.selected);
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  async fetchItems() {
    // get relevant accounts
    this.privilegedAccounts = await this._adminService.getPrivilegedAccounts(
      this.privilegeName,
      this.target
    );
    // decode masks
    this.privilegedAccounts = this._adminService.decodeMasksToPermissions(
      this.privilegedAccounts,
      this.maskMap,
      this.permissionsList
    );
    // reset initial state
    this.initialAccounts = JSON.parse(JSON.stringify(this.privilegedAccounts));
    this.privilegedAccounts.forEach((account) => {
      // used to update existing account masks
      account.toBeUpdated = true;
    });
    // set items
    this.tableEditor.set(this.privilegedAccounts);
  }

  undo() {
    this._modalService
      .showInfoDialog(
        this._translateService.instant('Admin.Permissions.UndoChanges'),
        this._translateService.instant('Admin.Permissions.UndoChangesMessage'),
        DIALOG_BUTTONS.OkCancel
      )
      .then((result) => {
        if (result === 'ok') {
          try {
            // undo changes
            this.tableEditor.undo();
            // rollback to initial checkbox states for existing accounts
            this.tableEditor.set(this.initialAccounts);
            // reset accounts
            this.privilegedAccounts = JSON.parse(
              JSON.stringify(this.initialAccounts)
            );
            this.privilegedAccounts.forEach((account) => {
              // used to update existing account masks.
              account.toBeUpdated = true;
            });
          } catch (err) {
            console.error(err);
            this._errorService.showError(err, {
              continueLink: '.',
            });
          }
        }
      });
  }
}
