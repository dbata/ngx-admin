export const MODIFICATIONS_HISTORY_LIST = {
  columns: [
    {
      name: 'dateCreated',
      property: 'dateCreated',
      title: 'Admin.ModificationHistory.DateCreated',
      formatter: 'DateTimeFormatter',
      formatString: 'short',
    },
    {
      name: 'title',
      property: 'title',
      title: 'Admin.ModificationHistory.EventTitle',
    },
    {
      name: 'username',
      property: 'createdBy',
      title: 'Admin.ModificationHistory.CreatedBy',
    },
  ],
};
