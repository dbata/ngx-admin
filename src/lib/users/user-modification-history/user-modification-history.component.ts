import {
  AfterViewInit,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { MODIFICATIONS_HISTORY_LIST } from './user-modification.config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-user-modification-history',
  templateUrl: './user-modification-history.component.html',
  styleUrls: ['./user-modification-history.component.css'],
})
export class UserModificationHistoryComponent
  implements AfterViewInit, OnDestroy {
  @ViewChild('table') table: AdvancedTableComponent;
  private subscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly context: AngularDataContext
  ) {}

  ngAfterViewInit() {
    this.subscription = this.activatedRoute.parent.params.subscribe(
      (params) => {
        const user = params.id;
        // set table query
        this.table.query = this.context
          .model('ObjectEventLogs')
          .where('objectType')
          .equal('User')
          .and('object')
          .equal(user.toString())
          .orderByDescending('dateCreated')
          .prepare();
        // set table config
        this.table.config = AdvancedTableConfiguration.cast(
          MODIFICATIONS_HISTORY_LIST
        );
        // and init
        this.table.fetch();
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
