import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AppEventService,ErrorService, TemplatePipe, ReferrerRouteParams, ReferrerRouteService} from '@universis/common';
import { AdvancedTableConfiguration } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { users_list } from '../users-list/users-list.config.list';

@Component({
  selector: 'lib-user-root',
  templateUrl: './user-root.component.html'
})
export class UserRootComponent implements OnInit, OnDestroy {
  public user: any;
  public isCreate = false;
  public config: any;
  public edit: any;
  public actions: any[];
  public allowedActions: any[];
  private changeSubscription: Subscription;
  referrerRoute: ReferrerRouteParams = {
    commands: ['/users']
  };
  referrerSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _errorService: ErrorService,
              private _template: TemplatePipe,
              public referrer: ReferrerRouteService) { }

  async ngOnInit() {
    this.config = AdvancedTableConfiguration.cast(users_list, true);
    if (this._activatedRoute.snapshot.params.id) {
      try {
        await this.loadData();
        this.populateActions();
      } catch (err) {
        console.error(err);
        this._errorService.navigateToError(err);
      }
    }
    this.changeSubscription = this._appEvent.change.subscribe(async event => {
      if (event && event.model === 'Users' && this._activatedRoute.snapshot.params.id) {
        try {
          await this.loadData();
          this.populateActions();
        } catch (err) {
          console.error(err);
          this._errorService.navigateToError(err);
        }
      }
    });
    this.referrerSubscription = this.referrer.routeParams$.subscribe(result => {
      this.referrerRoute = result || {
        commands: ['/users']
      };
    });
  }

  async loadData() {
    this.user = await this._context.model('Users')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
    this._appEvent.change.next({
      model: 'User',
      target: this.user
    });
  }

  populateActions(): void {
    if (this.config && this.config.columns && this.user) {
      // get actions from config file
      const actions = this.config.columns.filter((x: any) => {
        return x.actions;
      })
      // map actions
      .map((x: any) => x.actions)
      // get list items
      .reduce((_: any, b: any) => b, 0);
      if (!(Array.isArray(actions) && actions.length)) {
        return;
      }
      this.allowedActions = actions.filter((x: any) => {
        if (x.role !== 'action') {
          return;
        }
        if (x.access && x.access.length) {
          let accessAttributes = x.access;
          accessAttributes = accessAttributes.filter((item: any) => {
            if (item.hasOwnProperty('enabled')) {
              return item.enabled === !!(this.user.enabled);
            }
          });
          if (accessAttributes.length) {
            return x;
          }
        } else {
          return x;
        }
      });
      this.edit = actions.find((x: any) => {
        if (x.role === 'edit') {
          if (x.href) {
            x.href = this._template.transform(x.href, this.user);
          }
          return x;
        }
      });
      this.actions = this.allowedActions.map((action: any) => {
        if (action.href) {
          action.href = this._template.transform(action.href, this.user);
        }
        return action;
      });
    }
  }

  ngOnDestroy() {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
