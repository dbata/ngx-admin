/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const departments_list = {
  "title": "Admin.Groups.All",
  "model": "Departments",
  "searchExpression": "indexof(name, '${text}') ge 0",
  "selectable": true,
  "multiSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "title": "Admin.Departments.DepartmentId"
    },

    {
      "name": "name",
      "property": "name",
      "title": "Admin.Departments.Name"
    },
    {
      "name": "city",
      "property": "city",
      "title": "Admin.Departments.City"
    }
  ],
  "criteria": [
    {
      "name": "id",
      "filter": "(indexof(id, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ]
};
