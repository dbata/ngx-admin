import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AdvancedFormComponent} from '@universis/forms';
import {AppEventService, TemplatePipe, ToastService} from '@universis/common';

@Component({
  selector: 'lib-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  public user: any;
  public userId: any;
  @Input() data: any;
  @Input() src: any;
  @Input() continueLink: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _router: Router,
              private _template: TemplatePipe) {
  }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id;
    }

    this.user = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.userId) {
          this.user = event.target;

          if (this.user) {
            if (this.form) {
              this.form.ngOnDestroy();
            }
            this.src = `Users/edit`;
            this.data = this.user;
            if (this.form) {
              this.form.ngOnInit();
            }
          }
        }
      }
    });
  }

  async onCompletedSubmission($event: any) {
    if (this.form && this.form.form) {
      this.form.form.options.disableAlerts=true;
    }
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        if (typeof x === 'string') {
          return this._template.transform(x, this.form.data);
        }
        return x;
      });
    } else if (typeof this.continueLink === 'string' && this.continueLink.trim().length > 0) {
      continueLink = [this._template.transform(this.continueLink, this.form.data)];
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }
}
