/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const users_list = {
  "title": "Users",
  "model": "Users",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["../..", "${id}", "edit"]
      }
    },
    {
      "name": "id",
      "formatter": "ActionLinkFormatter",
      "hidden": true,
      "virtual": true,
      "actions" : [
        {
          "title": "Admin.Users.RemoveUserAction.Title",
          "href": "#/users/${id}/edit/(modal:remove)",
          "role": "action",
          "access": [
            {
              "enabled": true
            }
          ]
        }
      ]
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Users.AlternateName"
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Users.Name"
    },
    {
      "name": "description",
      "property": "description",
      "title": "Admin.Users.Description"
    },
    {
      "name": "enabled",
      "property": "enabled",
      "title": "Admin.Users.StatusTitle",
      "formatter": "TranslationFormatter",
      "formatString": "Admin.Users.Enabled.${value}"
    },
    {
      "name": "groups",
      "property": "groups",
      "virtual": true,
      "sortable": false,
      "title": "Admin.Groups.TitlePlural",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${groups.length ? groups.map(x => x.name).join(', ') : '-'}"
        },
        {
          "formatter": "TranslationFormatter",
          "formatString": "${value}"
        }
      ]
    },
    {
      "name": "dateCreated",
      "property": "dateCreated",
      "title": "Admin.Users.DateCreated",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    },
    {
      "name": "dateModified",
      "property": "dateModified",
      "title": "Admin.Users.DateModified",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    }
  ],
  "defaults": {
    "expand": "groups"
  },
  "criteria": [
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "enabled",
      "filter": "(enabled eq '${value}')",
      "type": "text"
    },
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "createdBefore",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "createdAfter",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')",
      "type": "text"
    }
  ],
  "paths" : [
    {
      "name": "Users.Enabled.Plural",
      "alternateName": "list/active",
      "show": true,
      "filter": {
        "enabled": "1"
      }
    },
    {
      "name": "Users.All",
      "show": true,
      "alternateName": "list/index",
      "filter": { }
    }

  ],
  "searches": []
};
