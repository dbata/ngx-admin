import {el} from './admin.el';
import {en} from './admin.en';

export const ADMIN_LOCALES = {
  el,
  en,
};
